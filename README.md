Mintonette Volleyball App
==========================
Real-time volleyball statistics app built with Polymer & Firebase.


## How To Develop With App
This app is built using the [yeoman-polymer](https://github.com/yeoman/generator-polymer) generator.

#### Set Up The Environment
Once you have forked and cloned the repo locally, `cd` into the directory run:

* `npm install`
* `bower install`

After downloading the required dependencies, use either command below for development:

* `grunt serve` for development mode
* `grunt test:local` for unit testing
* `grunt` to make a production build

--

## Tools and Technology
In order to successfully develop with this app, become familiar with the following technologies by reading the APIs. For Polymer, please read the *ENTIRE* [Getting Started](https://www.polymer-project.org/docs/start/getting-the-code.html) and [Guides & Resources](https://www.polymer-project.org/docs/polymer/polymer.html) sections.

#### Front-End
* [Polymer](https://www.polymer-project.org/) - Entire app framework and library
* [SASS](http://sass-lang.com/) - Preprocessor for CSS
* [WCT](https://github.com/Polymer/web-component-tester) - Unit testing library

–
#### Back-End
*Currently Nothing*


#### Databases & VM
* [Firebase](https://www.firebase.com/) - Real Time Database storage


#### Other
* Icons: [Material Design Icons](https://github.com/google/material-design-icons)


## Branch Design
In order to keep track of versions within the app, we will follow the flow plan from Vincent Driessen's [blog](http://nvie.com/posts/a-successful-git-branching-model/). Key points:

* When developing, use the `development` branch
* When working on new feature for app, merge from `development` into `features` and use the `features` branch
* When ready for a release, merge `development` into the `release` branch
* When working on hotfix for current release, merge `master` into `hotfix` and use the `hotfix` branch
* When ready to release version, merge `release` branch into `master` tagged with release version name
* RINSE, and REPEAT: merge `master` into `development` and start working on next version