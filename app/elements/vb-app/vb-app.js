// All code under GNU GENERAL PUBLIC LICENSE 2015, copyright 2015 Mintonette software agency

(function () {
  'use-strict';

  var debug = false,
      user = this.user,
      teamUID = this.teamUID,
      msgTimeout;

  window.setVballTransitionSpeed = function(ms) {
    CoreStyle.g.transitions.duration = ms + 'ms';
    CoreStyle.g.transitions.scaleDelay = CoreStyle.g.transitions.duration;  
  }

  setVballTransitionSpeed(350);

  // Vb-app script
  Polymer('vb-app', {
    route: 'splash',
    responsiveWidth: '900px',
    connected: false,
    minSplashTime: 1000,
    user: this.user,

    observe: {
      'user': 'startup'
    },

    ready: function() {
      var app = dummyState = {app: 'MintonetteVball'};

      // Make sure app not offline and defaults work
      this.test = window.location.search.indexOf('test') >= 0;
      this.offline = this.test || window.location.search.indexOf('offline') >= 0;
      this.readyTime = Date.now();

      window.addEventListener('offline', function() {
        Firebase.goOffline();
      });

      window.addEventListener('online', function() {
        Firebase.goOnline();
      });

      if (!this.user) {
        if (debug) {
          console.log('ready: no user exists')
        }

        this.showLogin();
      }
    },

    startup: function() {
      if (debug) {
        console.log('startup');
      }

      var elapsed = Date.now() - this.readyTime;
      var t = this.minSplashTime - elapsed;
      this.async('completeStartup', null, t > 0 ? t : 0);
    },

    completeStartup: function() {
      if (debug) {
        console.log('completeStartup');
      }

      // if user exists, go to home page
      if (this.user) {
        if (debug) {
          console.log('completeStartup User: ' + this.user.uid);
        }

        // create user obj
        // this.$.fbase.data = this.user;
        this.showHome();
      }

      else {
        this.showLogin();
      }
    },


    // navigateTo routes
    showLogin: function() {
      MoreRouting.navigateTo('/');
    },

    showHome: function() {
      MoreRouting.navigateTo('/home');
    },
    
    showProfile: function() {
      MoreRouting.navigateTo('/users');
      this.$.profile.userDefaults = this.user;
    },

    transitionEndAction: function() {
      if (debug) {
        console.log('transition ended');
      }

      this.style.height = '100vh';
      this.$.coreAnimatedPages.style.height = '100vh';
    }

  });
})();