/* All code under GNU GENERAL PUBLIC LICENSE 2015, copyright 2015 Mintonette software agency */

(function () {
  'use-strict';

  var debug = false,
      user = this.user,
      teamuid = this.teamuid,
      msgTimeout;

  // Vb-matchmode script
  Polymer('vb-matchmode', {
    offense: true,
    defense: false,

    observe: {
      'user': 'syncUser',
    },

    ready: function() {
      if (debug) {
        console.log('vb-matchmode loaded');
      }

    },

    // data sync
    syncUser: function() {
      if (debug) {
        console.log('matchmode syncUser: ' + this.$.fbLogin.user.uid);
      }

      this.user = this.$.fbLogin.user;
    },


    // navigateTo routes
    showLogin: function() {
      MoreRouting.navigateTo('/');
    },

    showHome: function() {
      MoreRouting.navigateTo('/home');
    },
    
    showProfile: function() {
      MoreRouting.navigateTo('/users');
    },

    showResults: function() {
      MoreRouting.navigateTo('/results');
    },


    // Firebase methods
    logout: function() {
      if (debug) {
        console.log('Logging out');
      }

      this.$.fbLogin.logout();
      this.showLogin();
    },


    // show choices
    showOffensive: function() {
      this.defense = false;
      this.offense = true;
    },

    showDefensive: function() {
      this.offense = false;
      this.defense = true;
    },


    // match plays
    logPlayer: function(e, detail, sender) {
      if (debug) {
        console.log('player: ' + sender.id);
      }

      var previousBtn = this.$.btnContainer.querySelector('paper-button.active');

      // clear all previous players selected
      if (previousBtn) {
        previousBtn.classList.remove('active');
        previousBtn.classList.add('primaryBtn');
      }

      // Add active state to current player
      sender.classList.remove('primaryBtn');
      sender.classList.add('active');

      return this.player = sender.id;
    },

    logMove: function(e, detail, sender) {
      if (debug) {
        console.log(this.selected);
      }

      return this.move = this.selected;
    },

    submitPlay: function() {
      var previousBtn = this.$.btnContainer.querySelector('paper-button.active');

      this.play = {};
      this.play.player = this.player;
      this.play.move = this.move;

      if (this.play.player == null) {
        this.message = 'You must select a player';
        this.$.toast1.style.backgroundColor = 'red';
        this.$.toast1.show();
      }

      else if (this.play.move == null) {
        this.message = 'You must select a rally';
        this.$.toast1.style.backgroundColor = 'red';
        this.$.toast1.show();
      }

      else {
        // save play to fb
        new Firebase('https://volleyball-app.firebaseio.com/' + this.user.uid + 
          '/userTeams/' + this.teamuid + '/matches/matchResults/').push(this.play);

        this.message = 'Player ' + this.play.player + ' rally submitted';
        this.$.toast1.style.backgroundColor = '#323232';
        this.$.toast1.show();
      }

      // clear all previous players selected
      previousBtn.classList.remove('active');
      previousBtn.classList.add('primaryBtn');

      // clear all checkboxes and player states
      this.player = null;
      this.selected = null;

      if (debug) {
        console.log(this.play);
      }
    },

    endSet: function(e, detail, sender) {
      if (debug) {
        console.log(this.endset);
      }

      this.play = {};
      this.play.move = 'SET ENDED';

      // save play to fb
      new Firebase('https://volleyball-app.firebaseio.com/' + this.user.uid + 
        '/userTeams/' + this.teamuid + '/matches/matchResults/').push(this.play);
    },

    endMatch: function() {
      this.showResults();
    }

  });
})();