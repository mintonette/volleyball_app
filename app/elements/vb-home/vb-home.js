// All code under GNU GENERAL PUBLIC LICENSE 2015, copyright 2015 Mintonette software agency

(function () {
  'use-strict';

  var debug = false,
      user = this.user,
      teamuid = this.teamUID,
      players = false,
      data;

  Polymer('vb-home', {

    observe: {
      'user': 'syncUser',
      'teamuid': 'syncTeam'
    },

    ready: function () {
      if (debug) {
        console.log('home page is ready!');
      }


    },

    syncUser: function() {
      if (debug) {
        console.log('home syncUser: ' + this.$.fbLogin.user.uid);
      }

      this.user = this.$.fbLogin.user;
    },

    syncTeam: function() {
      if (debug) {
        console.log('home syncTeams: ' + this.teamuid);
      }

      this.teamuid = this.teamUID;
    },


    // navigateTo routes
    showLogin: function() {
      MoreRouting.navigateTo('/');
    },

    showHome: function() {
      MoreRouting.navigateTo('/home');
    },
    
    showProfile: function() {
      MoreRouting.navigateTo('/users');
    },

    showMatch: function() {
      MoreRouting.navigateTo('/matchmode');
    },


    // Firebase methods
    logout: function() {
      if (debug) {
        console.log('Logging out');
      }

      this.$.fbLogin.logout();
      this.showLogin();
    },

    error: function(e) {
      if (debug) {
        console.log('User Error');
      }

      this.messages = 'Error: ' + e.detail.message;
      this.$.toast1.style.backgroundColor = 'red';
      this.$.toast1.show();
    },


    // Open Dialog's
    open: function(e) {
      if (debug) {
        console.log('opening add team dialog');
      }

      this.$.addTeamDialog.toggle();
    },

    openInfo: function(e) {
      if (debug) {
        console.log('opening more info dialog');
      }

      this.$.infoDialog.toggle();
    },

    // Loop through data selection
    regionSelected: function() {
      if (debug) {
        console.log(this.regionSelection);
      }

      var convert = [];
      // save selected region
      this.region = this.regionSelection;

      // Loop through ajax data to find obj selected
      for (var i = 0; i <= this.regionData.length - 1; i++) {
        if (this.regionData[i].name === this.region) {
          this.regionObj = this.regionData[i];
          // convert obj to array b/c Polymer doesn't loop -> obj
          convert.push(this.regionObj);
          this.convert = convert;
        }
      }

      return this.regionSelection;
    },

    clubSelected: function(e, detail) {
      if (debug) {
        if (detail.isSelected) {
          this.selectedItem = detail.item.textContent;
          console.log(this.selectedItem);
        }
      }
      // define the item selected
      if (detail.isSelected) {
        this.selectedItem = detail.item.textContent;
      }

      var clubConvert = [];
      // save selected region
      this.club = this.selectedItem;

      // Loop through convert data to find obj selected
      for (var i = 0; i <= this.convert.length - 1; i++) {
        var clubs = this.convert[i].clubs;

        for (var j = 0; j <= clubs.length - 1; j++) {
          if (clubs[j].name === this.club) {
            this.clubObj = clubs[j];
            // clubs obj to array b/c Polymer doesn't loop -> obj
            clubConvert.push(this.clubObj);
            this.clubConvert = clubConvert;
          }
        }
      }

      return this.club;
    },

    selectAction: function(e, detail) {
      if (detail.isSelected) {
        this.selectedItem = detail.item.textContent;
      }

      if (debug) {
        console.log(this.selectedItem);
      }

      return this.selectedItem;
    },

    addTeam: function() {
      this.teams = [];
      this.team = {};

      this.team.region = this.regionSelection;
      this.team.state = this.stateSelection;
      this.team.club = this.club;
      this.team.team = this.selectedItem;
      this.teams.push(this.team);

      // Add team to users firebase
      this.$.fbase.push(this.team);

      // Show toast when team added
      this.message = 'Added team: ' + this.selectedItem + ' to account';
      this.$.toast1.show();

      // Save all teams to localStorage
      localStorage.setItem('vballTeams', this.teams);

      if (debug) {
        console.log('Added Team: ' + this.team.team);
      }

      // clear out dropboxes in case of adding new team
      this.regionSelection = ''
      this.stateSelection = ''
      this.clubSelection = ''
      this.teamSelection = ''
    },

    // open confirmation dialog to approve team deletion
    deleteTeam: function(e, detail, sender) {
      if (debug) {
        console.log('confirm delete team: ' + this.teamUID);
      }

      this.teamUID = sender.id;

      this.$.confirmDialog.toggle();
      return this.teamUID;
    },

    // exterminate team
    confirmTeamDelete: function() {
      if (debug) {
        console.log('deleted team: ' + this.teamUID);
      }

      this.$.fbase.remove(this.teamUID);
    },

    addNewPlayer: function(e, detail, sender) {
      if (debug) {
        console.log('team: ' + sender.id);
      }

      this.teamUID = sender.id;

      // Add if conditional to show dialog if no players and go to home if are

      this.$.playersDialog.toggle();
      return this.teamUID;
    },

    addPlayer: function(e, detail) {
      // Get content of dropdown selection
      if (detail.isSelected) {
        this.selectedItem = detail.item.textContent;
      }

      // Validate if playerNumber is correct type
      if (typeof this.playerNumber !== 'number') {
        this.message = 'Players number must be a valid number';
        this.$.toast1.style.backgroundColor = 'red';
        this.$.toast1.show();
      }

      if (debug) {
        console.log('this team: ' + this.teamUID);
        console.log('added player: ' + this.player);
      }

      // Create player array to hold players obj
      this.players = [];
      this.player = {};
      this.player.fname = this.playerFirstName;
      this.player.lname = this.playerLastName;
      this.player.number = this.playerNumber;
      this.player.position = this.selectedItem;
      this.players.push(this.player);

      new Firebase('https://volleyball-app.firebaseio.com/' + this.user.uid + '/userTeams/' + this.teamUID + '/players')
        .push(this.player);

      // Show toast success message
      this.message = 'Added player ' + this.playerNumber + ' to team!';
      this.$.toast1.style.backgroundColor = '#323232';
      this.$.toast1.show();

      // Clear out form for next player
      this.playerFirstName = null;
      this.playerLastName = null;
      this.playerNumber = null;
      this.position = null;
    },

    getTeamUid: function(e, detail, sender) {
      if (debug) {
        console.log('team: ' + sender.id);
      }

      this.teamUID = sender.id;

      return this.teamuid = this.teamUID;
    },

  });

})();