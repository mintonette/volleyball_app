// All code under GNU GENERAL PUBLIC LICENSE 2015, copyright 2015 Mintonette software agency

(function () {
  'use-strict';

  var msgTimeout,
      messages,
      user = this.user,
      params,
      debug = false;

  Polymer('vb-login', {

    // tab through the input fields on "Enter"
    keypressAction: function(ev) {
      var code = ev.keyCode || ev.charCode;
      var key = ev.keyIdentifier;

      if (key === 'Enter' || code === 13) {
        var target = ev.target;

        if (target === this.$.first) {
          // TODO(dfreedm): calling the focus event handler in lieu of a real api
          this.$.userPassword.focusAction();
        } 
        else if (target === this.$.userPassword) {
          // after "userPassword" field, lower keyboard to show avatar list
          this.$.userPassword.blur();
        }
      }
    },

    ready: function () {
      var statusKnown = this.$.fbLogin.statusKnown,
            user = this.$.fbLogin.user;

      if (debug) {
        console.log('login statusKnown: ' + statusKnown);
      }

      // redirect to home page if user already logged in
      if (statusKnown || user) {
        this.showHome();
      }

      // Show intial state of user
      this.$.toast1.show();
    },


    // navigateTo routes
    showLogin: function() {
      MoreRouting.navigateTo('/');
    },

    showHome: function() {
      MoreRouting.navigateTo('/home');
    },


    // Firebase methods
    login: function() {
      if (debug) {
        console.log('Attempt to login()');
        console.log('provider: ' + this.$.fbLogin.provider);
        console.log('email: ' + this.email);
        console.log('password: ' + this.userPassword);
      }

      if (this.$.fbLogin.provider == 'password') {
        params = this.params || {};
        params.email = this.email;
        params.password = this.userPassword;
      }

      this.$.fbLogin.login(params);
    },

    logout: function() {
      this.user = null;
      this.$.fbLogin.logout();
    },

    createUser: function() {
      if (debug) {
        console.log('Creating User');
      }

      this.user = {};
      this.user.email = this.email;
      this.user.password = this.userPassword;

      this.$.fbLogin.createUser(this.email, this.userPassword);
      this.$.fbase.data = this.user;
      this.$.toast1.show();
    },

    removeUser: function() {
      if (debug) {
        console.log('Removing User');
      }

      this.$.fbLogin.removeUser(this.email, this.userPassword);
    },

    changePassword: function() {
      if (debug) {
        console.log('Changing password');
      }

      this.$.fbLogin.changePassword(this.email, this.userPassword, this.newPassword);
    },

    resetPassword: function() {
      if (debug) {
        console.log('Reseting Password');
      }

      this.$.fbLogin.sendPasswordResetEmail(this.email);
    },

    // if there was an error with log in
    error: function(e) {
      if (debug) {
        console.log('Login Error');
      }

      e.preventDefault();
      this.messages = 'Error: ' + e.detail.message;
      this.$.toast1.style.backgroundColor = 'red';
      this.$.toast1.show();
    },
    
    // If creating user or logging in was successful
    userSuccess: function() {
      if (debug) {
        console.log('Successfully created user');
      }

     this.showHome();
    }

  });
})();