// All code under GNU GENERAL PUBLIC LICENSE 2015, copyright 2015 Mintonette software agency

(function () {
  'use-strict';

  var debug = false;

  Polymer('vb-data', {
    observe: {
      'fbase userReady': 'syncUser'
    },

    ready: function () {
      if (debug) {
        console.log('User: ' + this.$.fbLogin.user);
        console.log('Provider: ' + this.$.fbLogin.provider);
        console.log('Returning User: ' + localStorage.vballUser);
      }

      // check if online or offline with defaults
      this.test = window.location.search.indexOf('test') >= 0;
      this.offline = this.test || window.location.search.indexOf('offline') >= 0;

      if (localStorage.vballUser) {
        this.user = {
          email: localStorage.vballUser.email
        }
      }

      // Show initial state of user
      this.$.toast1.show();

      if (this.test) {
        this.user = {
          name: 'testUser',
          team: 'testTeam'
        };
      }

      else {
        try {
          this.user = JSON.parse(localStorage.getItem('vball-user'))
        }
        catch (e) {
          this.user = null;
          this.$.toast1.e.detail;
        }
      }

      // Take firebase offline/online
      window.addEventListener('offline', function() {
        Firebase.goOffline();
      });

      window.addEventListener('online', function() {
        Firebase.goOnline();
      });

    },

    error: function(e) {
      if (debug) {
        console.log('User Error');
      }

      this.messages = 'Error: ' + e.detail.message;
      this.$.toast1.style.backgroundColor = 'red';
      this.$.toast1.show();
    },

    userSuccess: function(e) {
      if (debug) {
        console.log('Successfully created user');
      }

      this.messages = e.type + 'success!';
      this.$.toast1.show();
    },

    removeUser: function() {
      if (debug) {
        console.log('Removing User');
      }

      this.$.fbLogin.removeUser(this.user);
    },

    changePassword: function() {
      if (debug) {
        console.log('Changing password');
      }

      this.$.fbLogin.changePassword(this.email, this.password, this.newPassword);
    },

    resetPassword: function() {
      if (debug) {
        console.log('Reseting Password');
      }

      this.$.fbLogin.sendPasswordResetEmail(this.email);
    },

    // Update info
    statusKnownChanged: function() {
      if (debug) {
        console.log('statusKnownChanged');
      }

      if (this.statusKnown && !this.offline && !this.$.fbase) {
        this.$.fbLogin.login(this.user);
      }
    },

    syncUser: function() {
      if (debug) {
        if (this.user) {
          console.log('syncUser: ' + this.user.email);
        }
        else {
          console.log('else syncUser: ' + this.$.fbase.data);
        }
      }

      if (this.$.fbase && this.userReady) {
        this.$.fbase.data = this.user;
      } 

      else {
        this.$.fbase.data = null;
      }
    },

    userChanged: function() {
      // if no user - logout
      if (!this.user) {
        if (debug) {
          console.log('userChanged: No user');
        }

        this.logout();
      }

      // else, sync the user's data
      this.syncUser();
      this.observeUser();
      this.userFieldsChanged();
    },

    observeUser: function() {
      if (this.userObserver) {
        this.userObserver.close();
        this.userObserver = null;
      }

      if (this.user) {
        this.userObserver = new ObjectObserver(this.user);
        this.userObserver.open(this.userFieldsChanged.bind(this));
      }
    },

    userFieldsChanged: function(added, removed, changed, getOldValueFn) {
      localStorage.setItem('vballUser', JSON.stringify(this.user.email));
    },

    logout: function() {
      this.$.fbLogin.logout();
      this.$.fbLogin.login();
    }

  });

})();