/* All code under GNU GENERAL PUBLIC LICENSE 2015, copyright 2015 Mintonette software agency */

(function () {
  'use-strict';

  var debug = false,
      user = this.user,
      teamuid = this.teamuid,
      msgTimeout;

  // Vb-dashboard script
  Polymer('vb-dashboard', {

    observe: {
      'user': 'syncUser',
    },

    ready: function() {
      if (debug) {
        console.log('vb-dashboard loaded');
      }

    },


    // data sync
    syncUser: function() {
      if (debug) {
        console.log('dashboard syncUser: ' + this.$.fbLogin.user.uid);
      }

      this.user = this.$.fbLogin.user;
    },


    // navigateTo routes
    showLogin: function() {
      MoreRouting.navigateTo('/');
    },

    showHome: function() {
      MoreRouting.navigateTo('/home');
    },
    
    showProfile: function() {
      MoreRouting.navigateTo('/users');
    },

    showResults: function() {
      MoreRouting.navigateTo('/results');
    },


    // Firebase methods
    logout: function() {
      if (debug) {
        console.log('Logging out');
      }

      this.$.fbLogin.logout();
      this.showLogin();
    }

  });
})();